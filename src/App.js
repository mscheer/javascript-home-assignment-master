import React from 'react';
import './App.scss';
import EmployeeList from "./components/EmployeeList";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Employee from "./components/Employee";

function App() {


    return (
        <div className="App">
          <Router>
            <header className="App-header">
                <h1>Employee list</h1>
            </header>
            <div className="Content">

                <Routes>
                  <Route exact path={'/'} element={<EmployeeList/>}/>
                  <Route exact path={'/employee/:id'} element={<Employee/>}/>
                </Routes>

            </div>
        </Router>
        </div>
    );
}

export default App;
