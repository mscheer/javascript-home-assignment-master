import React, {useEffect, useState} from "react";
import axios from "axios";
import {useParams} from "react-router-dom";

function Employee() {
	const [employeeObject, setEmployeeObject] = useState({});

	let {id} = useParams();

	const dateOptions = {
		year: 'numeric',
		month: 'long',
		day: 'numeric'
	};

	useEffect(() => {
		axios.get(`http://localhost:3001/employees/${id}`).then((response) => {
			setEmployeeObject(response.data);
		})
	}, [id])

	return (
		<div className={'employeeDetail'}>
			<p><label>ID: </label>{employeeObject.id}</p>
			<p><label>Name: </label>{employeeObject.name}</p>
			<p><label>Age: </label>{employeeObject.age}</p>
			<p><label>Department: </label>{employeeObject.departmentId}</p>
			<p><label>Start Date: </label>
				{new Date(employeeObject.startDate).toLocaleDateString('sk-SK', dateOptions)}
			</p>
		</div>
	)
}

export default Employee;
