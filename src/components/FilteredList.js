import React from "react";
import {useNavigate} from "react-router-dom";


function FilteredList({id, name, departmentId}) {

	let navigate = useNavigate();

	return (
		<tr key={id}>
			<td>
				{id}
			</td>
			<td className={'employeeName'} onClick={() => { navigate(`/employee/${id}`)}} >
				{name}
			</td>
			<td>{departmentId}</td>
		</tr>
	)
}

export default FilteredList;
