import React, {useEffect, useMemo, useState} from 'react';
import axios from "axios";
import FilteredList from "./FilteredList";

function EmployeeList() {

    const [listOfEmployees, setListOfEmployees] = useState([]);
    const [listOfDepartment, setListOfDepartment] = useState([]);
    const [selectedId, setSelectedId] = useState();

    useEffect( () => {
        axios.get("http://localhost:3001/departments").then((response) => {
            setListOfDepartment(response.data);
        });
        axios.get("http://localhost:3001/employees").then((response) => {
            setListOfEmployees(response.data);
        });
    }, []);

    function getFilteredList() {
        if (!selectedId) {
            return listOfEmployees;
        }
        return listOfEmployees.filter((item) => (item.departmentId * 1) === (selectedId * 1));
    }

    let filteredList = useMemo(getFilteredList, [selectedId, listOfEmployees]);

    function handleIdChange(event) {
        setSelectedId(event.target.value);
    }

    return (
      <>
          <div className="Department-Filter">
              <select onChange={handleIdChange}>
                  <option value={''}>All departments</option>
                  {
                      listOfDepartment.map((department, key) => {
                          return (
                            <option key={key} value={department.id}>{department.departmentName}</option>
                          )
                      })
                  }
              </select>
          </div>
          <div className={"Employee-List"}>
              <table>
                  <thead>
                  <tr>
                      <th>Employee ID</th>
                      <th>Name</th>
                      <th>Department</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                      filteredList.map((employee, key) => (
                        <FilteredList {...employee} key={key} />
                      ))
                  }
                  </tbody>
              </table>
          </div>
      </>

    )
}

export default EmployeeList;
